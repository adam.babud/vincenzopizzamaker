package model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class PizzaTopping extends PizzaMaterial {
    
    public static Map<String,PizzaTopping> toppings = new HashMap<>();

    public PizzaTopping(String name, double price) {
        super(name, price);
    }

    public static void addPizzaToppingToMap(String name, double price){
        PizzaTopping t = PizzaTopping.toppings.get(name);
        if(t==null){
            t=new PizzaTopping(name, price);
            PizzaTopping.toppings.put(name, t);
        }
    }
    
    public void removePizzaTopping(PizzaTopping topping){
        toppings.remove(topping.getName(), topping);
    }
    
    public static PizzaTopping getPizzaTopping(String toppingName){
        PizzaTopping t = PizzaTopping.toppings.get(toppingName);
        return t;
    }
    
    public static Collection<PizzaTopping> getPizzaToppings(){
	return toppings.values();
    }
    
    @Override
    public int compareTo(PizzaMaterial m) {
        return getName().compareTo(m.getName());
    }

    @Override
    public String toString() {
        return super.getName();
    }
    
}
