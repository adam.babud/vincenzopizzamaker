package model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class PizzaBase extends PizzaMaterial {
    
    public static Map<String,PizzaBase> bases = new HashMap<>();

    public PizzaBase(String name, double price) {
        super(name, price);
    }   

    public static void addPizzaBaseToMap(String name, double price){
        PizzaBase b = PizzaBase.bases.get(name);
        if(b==null){
            b=new PizzaBase(name, price);
            PizzaBase.bases.put(name, b);
        }
    }
    
    public static void removePizzaBase(PizzaBase base){
        bases.remove(base.getName(), base);
    }
    
    public static PizzaBase getPizzaBase(String baseName){
        PizzaBase b = PizzaBase.bases.get(baseName);
        return b;
    }
    
    public static Collection<PizzaBase> getPizzaBases(){
	return bases.values();
    }
    
    @Override
    public int compareTo(PizzaMaterial m) {
        return getName().compareTo(m.getName());
    }
    
    @Override
    public String toString() {
        return super.getName();
    }
}
