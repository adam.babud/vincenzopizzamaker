package model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class PizzaDough extends PizzaMaterial implements Cooking {
    
    public static final int AVG_TIME_OF_COOKING = 10;
    public static final double THICK_OF_THIN_DOUGH = 1.2;
    public static final double THICK_OF_NORMAL_DOUGH = 1.4;
    public static final double THICK_OF_THICK_DOUGH = 1.6;
    private double doughThickness;
    
    public static Map<String,PizzaDough> doughs = new HashMap<>();
    
    public PizzaDough(String name, double price) {
        super(name, price);
        switch(name){
            case "vékony tészta" : this.doughThickness = THICK_OF_THIN_DOUGH; break;
            case "normál tészta" : this.doughThickness = THICK_OF_NORMAL_DOUGH; break;
            case "vastag tészta" : this.doughThickness = THICK_OF_THICK_DOUGH; break;
            default : this.doughThickness = 1.4;
        }
    }

    public double getDoughThickness() {
        return doughThickness;
    }

    public void setDoughThickness(double doughThickness) {
        this.doughThickness = doughThickness;
    }
    
    public static void addPizzaDoughToMap(String name, double price){
        PizzaDough d = PizzaDough.doughs.get(name);
        //Ha nincs akkor létrehozunk egy újat és beletesszük a Map-be.
        if(d==null){
            d=new PizzaDough(name, price);
            PizzaDough.doughs.put(name, d);
        }
    }
    
    public static void removePizzaDough(PizzaDough dough){
        doughs.remove(dough.getName(), dough);
    }
    
    public static PizzaDough getPizzaDough(String doughName){
        PizzaDough d = PizzaDough.doughs.get(doughName);
        return d;
    }
    
    public static Collection<PizzaDough> getPizzaDoughs(){
	return doughs.values();
    }

    @Override
    public double getCookingTime(PizzaDough dough) {
        return dough.getDoughThickness()*AVG_TIME_OF_COOKING;
    }

    @Override
    public int compareTo(PizzaMaterial m) {
        return getName().compareTo(m.getName());
    }

    @Override
    public String toString() {
        return super.getName();
    }
    
}
