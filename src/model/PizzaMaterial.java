package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Map;

public abstract class PizzaMaterial implements Serializable, Comparable<PizzaMaterial>{

    private String name;
    private double price;

    public PizzaMaterial(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return getName();
    }
    

    public static void savePizzaMaterials(Map<String, ?> map, String savedName){
        File f = new File("src/db/"+savedName+".dat");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(map);
        } catch (IOException e) {
            e.printStackTrace();
        } finally{
            if(fos!=null){
                try {
                        fos.close();
                } catch (IOException e) {
                        e.printStackTrace();
                }
            }
        }
    }
	
    public static void loadPizzaMaterials(Map<String, ?> map, String savedName) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("src/db/"+savedName+".dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
            map = (Map<String, ?>)ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally{
            if(fis!=null){
                try {
                        fis.close();
                } catch (IOException e) {
                        e.printStackTrace();
                }
            }
        }
        switch (savedName) {
            case "doughs" : PizzaDough.doughs = (Map<String, PizzaDough>) map; break;
            case "bases" : PizzaBase.bases = (Map<String, PizzaBase>) map; break;
            case "toppings" : PizzaTopping.toppings = (Map<String, PizzaTopping>) map; break;
        }
    }

}
