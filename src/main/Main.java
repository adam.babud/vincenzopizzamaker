package main;

import service.PizzaDataBaseService;
import service.PizzaService;

public class Main {
    public static void main(String[] args) {
        PizzaDataBaseService pdbs = new PizzaDataBaseService();
        PizzaService ps = new PizzaService();
        pdbs.initPizzaDataBase();

        pdbs.saveUsers();
        pdbs.savePizzaMaterials();
        ps.makeGUI();
    }
}
