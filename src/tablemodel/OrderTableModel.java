package tablemodel;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.Order;

public class OrderTableModel extends AbstractTableModel {

	private static final String[] COLUMNS_NAME = new String[]{"Rendelés szám","Megrendelő","Cim","Telefon","Dátum","Megjegyzés","Ár","Mennyiség","Méret","Választott tészta","Választott alap","Választott feltétek","Extrák"};
	private List<Order> orders;
	
	public OrderTableModel(List<Order> orders){
		this.orders = orders;
	}
	
	@Override
	public int getRowCount() {
            return orders.size();
	}

	@Override
	public int getColumnCount() {
            return COLUMNS_NAME.length;
	}

	@Override
	public Object getValueAt(int row, int column) {
            Order order = orders.get(row);
            switch(column){
                case(0) : return order.getOrderId();
                case(1) : return order.getUser().getName();
                case(2) : return order.getUser().getAddress();
                case(3) : return order.getUser().getPhone();
                case(4) : return order.getDateOfOrder();
                case(5) : return order.getNote();
                case(6) : return order.getPriceOfOrder();
                case(7) : return order.getPizzaQuantity();
                case(8) : return order.getSize();
                case(9) : return order.getDough();
                case(10) : return order.getBase();
                case(11) : return order.getToppingsFormatted();
                case(12) : return order.getExtrasFormatted();
                default : return null;
            }
	}
	
	@Override
	public String getColumnName(int column){
		return COLUMNS_NAME[column];
	}
        
        public void addRow(Order order){
            orders.add(order);
            int row = orders.size()-1;
            fireTableRowsInserted(row, row);
        }
        
        public void deleteLastRow(){
            int lastRow = orders.size()-1;
            orders.remove(orders.get(lastRow)); 
            fireTableRowsDeleted(lastRow, lastRow);
        }
        
        public void refreshRows(List<Order> orders){
            this.orders = orders;
            fireTableDataChanged();
        }

        public void setOrders(List<Order> orders) {
            this.orders = orders;
        }
}

