package tablemodel;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.PizzaBase;
import model.PizzaMaterial;

public class BaseTableModel extends AbstractTableModel{
    private List<PizzaBase> bases;
    private static final List<String> COLUMN_NAMES = new ArrayList<>();
    private static final List<Class<?>> COLUMN_CLASSES = new ArrayList<>();
    
    static{
        COLUMN_NAMES.add("Name");
        COLUMN_NAMES.add("Price");
        
        COLUMN_CLASSES.add(String.class);
        COLUMN_CLASSES.add(String.class);
    }

    public BaseTableModel(List<PizzaBase> base) {
        this.bases = base;
        
    }
    
    public void addRow(PizzaBase base){
        bases.add(base);
        int row = bases.size()-1;
        fireTableRowsInserted(row,row);
    }
    public void deleteRow(PizzaBase base){
        int row = bases.indexOf(base);
        bases.remove(base); 
        fireTableRowsDeleted(row, row);
    }
    public void updateRow(PizzaBase base){
        int row = bases.indexOf(base);
        fireTableRowsUpdated(row, row);
    }
    public void modifyPrice(PizzaBase base, Double newPrice){
        int row = bases.indexOf(base);;
        bases.get(row).setPrice(newPrice);
        fireTableRowsDeleted(row, row);
    }

    @Override
    public String getColumnName(int column) {
        return COLUMN_NAMES.get(column);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return COLUMN_CLASSES.get(columnIndex);
    }
    
    
    @Override
    public int getRowCount() {
        return bases.size();
    }

    @Override
    public int getColumnCount() {
        return PizzaMaterial.class.getDeclaredFields().length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case(0) : return bases.get(rowIndex).getName();
            case(1) : return bases.get(rowIndex).getPrice();
            default: return null;
        }
    }
    
}

