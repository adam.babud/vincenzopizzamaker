package tablemodel;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.User;

public class UserTableModel extends AbstractTableModel {

	private static final String[] COLUMNS_NAME = new String[]{"Név","Jelszó","Cim","Telefon","Admin-e"};
	private List<User> users;
	
	public UserTableModel(List<User> users){
		this.users = users;
	}
	
	@Override
	public int getRowCount() {
            return users.size();
	}

	@Override
	public int getColumnCount() {
            return COLUMNS_NAME.length;
	}

	@Override
	public Object getValueAt(int row, int column) {
            User user = users.get(row);
            switch(column){
                case(0) : return user.getName();
                case(1) : return user.getPassword();
                case(2) : return user.getAddress();
                case(3) : return user.getPhone();
                case(4) : return user.isIsAdmin();
                default : return null;
            }
	}
	
	@Override
	public String getColumnName(int column){
		return COLUMNS_NAME[column];
	}
        
         public void addRow(User user){
            users.add(user);
            int row = users.size()-1;
            fireTableRowsInserted(row,row);
        }
        public void deleteRow(User user){
            int row = users.indexOf(user);
            users.remove(user); 
            fireTableRowsDeleted(row, row);
        }
        public void updateRow(User user){
//            int row = users.indexOf(user);
//            fireTableRowsUpdated(row, row);
            fireTableDataChanged();
        }
        public void refreshRows(){
            if(!User.getUsers().equals(null)){
                System.out.println("User tábla frissités.");
                List<User> userLista = new ArrayList<>(User.getUsers());
                setUsers(userLista);
                fireTableDataChanged();
            }
        }

        public void setUsers(List<User> users) {
            this.users = users;
        }
        
}
